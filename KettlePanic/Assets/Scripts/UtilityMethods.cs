﻿using UnityEngine;
using UnityEngine.UI;

namespace CauldronCooking
{
        public class WaitForLayoutCompletion : CustomYieldInstruction
        {
            public override bool keepWaiting
            {
                get
                {
                    return CanvasUpdateRegistry.IsRebuildingLayout();
                }
            }
        }

}
