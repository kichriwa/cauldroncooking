﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace CauldronCooking
{
    public class DragLogic : MonoBehaviour
    {
        public bool DragBlocked { get; set; }
        private Camera cam;

        void Awake()
        {
            OnValidate();
        }

        private void OnValidate()
        {
            cam = Camera.main;
        }

        public void HandleDragBegin(GameObject sender, PointerEventData eventData)
        {
            if (!DragBlocked)
            {
                gameObject.SetActive(true);
            }
        }

        public void HandleDrag(GameObject sender, PointerEventData eventData)
        {
            var screenPos = new Vector3(eventData.position.x, eventData.position.y);
            var worldPos = cam.ScreenToWorldPoint(screenPos);
            worldPos.z = 0;
            transform.position = worldPos;
        }

        public void HandleDragEnd(GameObject sender, PointerEventData eventData)
        {
            gameObject.SetActive(false);
        }
    }
}
