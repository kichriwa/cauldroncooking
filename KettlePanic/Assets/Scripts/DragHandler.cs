﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace CauldronCooking
{
    public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] public PointerEvent beginDragEvents;
        [SerializeField] public PointerEvent dragEvents;
        [SerializeField] public PointerEvent endDragEvents;

        public void OnBeginDrag(PointerEventData eventData)
        {
            beginDragEvents?.Invoke(gameObject, eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            dragEvents?.Invoke(gameObject, eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            endDragEvents?.Invoke(gameObject, eventData);
        }
    }
}