﻿using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.EventSystems;
using Image = UnityEngine.UI.Image;
using Vector3 = UnityEngine.Vector3;

namespace CauldronCooking
{
    public class Cauldron : MonoBehaviour
    {
        [SerializeField] private Image[] ingredientImages;
        [SerializeField] private Sprite slimeSprite;
        [SerializeField] private ParticleSystem[] splashEffects;

        private Dictionary<Ingredient, int> ingredients = new Dictionary<Ingredient, int>();
        private int addedIngredients;
        private Camera cameraReference;

        private void Awake()
        {
            OnValidate();
        }

        private void OnValidate()
        {
           cameraReference = Camera.main;
        }

        public void HandleDroppedObject(GameObject _, PointerEventData data)
        {
            if (data.pointerDrag.CompareTag("Ingredient"))
            {
                HandleReceivedItem(data.pointerDrag.GetComponentInParent<Item>());
                ExecuteNearestSplashEffect(data.position);
            }
            else if (data.pointerDrag.CompareTag("Portal"))
            {
                HandleReceivedItem(data.pointerDrag.GetComponentInChildren<Item>());
                ExecuteNearestSplashEffect(data.position);
            }
        }

        public void Empty()
        {
            ingredients = new Dictionary<Ingredient, int>();
            addedIngredients = 0;

            foreach (var image in ingredientImages)
            {
                image.gameObject.SetActive(false);
            }
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Bottle"))
            {
                other.GetComponent<BottleManager>().SetRecipe(GetCurrentRecipe());
            }
        }

        public Recipe GetCurrentRecipe()
        {
            foreach (Recipe recipe in GameManager.Instance.GetLevelData().recipes)
            {
                if (recipe.Equals(ingredients))
                {
                    return recipe;
                }
            }

            return null;
        }


        private void HandleReceivedItem(Item item)
        { 
           // splashEffects[Random.Range(0, splashEffects.Length)].Play();
            if (addedIngredients < ingredientImages.Length)
            {
                ingredientImages[addedIngredients].overrideSprite = item.GetComponent<Image>().overrideSprite;
                ingredientImages[addedIngredients].gameObject.SetActive(true);
            }
            else if (addedIngredients == ingredientImages.Length)
            {
                foreach (var ingredientImage in ingredientImages)
                {
                    ingredientImage.overrideSprite = slimeSprite;
                }
            }

            AddIngredient(item.Ingredient);
            item.GotUsed();
        }

        private void ExecuteNearestSplashEffect(Vector3 dropPosition)
        {
            ParticleSystem nearestSplash = splashEffects[0];
            float splashDistance = Mathf.Abs(dropPosition.x - cameraReference.WorldToScreenPoint(nearestSplash.transform.position).x);

            for (var i = 1; i < splashEffects.Length; i++)
            {
                float currentSplashDistance = Mathf.Abs(dropPosition.x - cameraReference.WorldToScreenPoint(splashEffects[i].transform.position).x);
                if (currentSplashDistance < splashDistance)
                {
                    splashDistance = currentSplashDistance;
                    nearestSplash = splashEffects[i];
                }
            }

            nearestSplash.Play();
        }

        private void AddIngredient(Ingredient ing)
        {
            addedIngredients++;
            if (ingredients.ContainsKey(ing))
            {
                ingredients[ing] += 1;
            }
            else
            {
                ingredients[ing] = 1;
            }
        }
    }
}