﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace CauldronCooking
{
    public class LevelManager : MonoBehaviour
    {
        private const int levelDurationSeconds = 10;
        [SerializeField] private CupboardManager cupboardManager;
        [SerializeField] private OrderManager orderManager;
        [SerializeField] private TextMeshProUGUI timer;
        [SerializeField] private GameObject levelEndScreen;
        [SerializeField] private TextMeshProUGUI finalScore;
        [SerializeField] private TextMeshProUGUI currentHighscore;
        [SerializeField] private TextMeshProUGUI newHighscoreText;
        private WaitForSeconds oneSecondTimeout;
        private Level levelData;

        void Start()
        {
            oneSecondTimeout = new WaitForSeconds(1);
            levelData = GameManager.Instance.GetLevelData();
            cupboardManager.InitializeCupboards(levelData.recipes);
            orderManager.Initialize(levelData.recipes, levelData.recipeDelay, levelData.failedTime);
            orderManager.MakeNewOrder();
            orderManager.StartOrderingCoroutine();
            StartCoroutine(LevelTimer());
        }

        public void RestartLevel()
        {
            levelEndScreen.SetActive(false);
            orderManager.MakeNewOrder();
            orderManager.StartOrderingCoroutine();
            StartCoroutine(LevelTimer());
        }

        public void BackToMainMenu()
        {
            GameManager.Instance.SwitchToScene(0);
        }

        private IEnumerator LevelTimer()
        {
            int secondsLeft = levelDurationSeconds;
            UpdateTimer(secondsLeft);

            while (secondsLeft > 0)
            {
                yield return oneSecondTimeout;
                secondsLeft--;
                UpdateTimer(secondsLeft);
            }

            ShowLevelEndScreen();
        }

        private void UpdateTimer(int secondsLeft)
        {
            var minutes = secondsLeft / 60;
            var seconds = secondsLeft % 60;

            timer.SetText($"{minutes}:{seconds:D2}");
        }

        private void ShowLevelEndScreen()
        {
            if (orderManager.CurrentScore > GameManager.Instance.CurrentHighscore)
            {
                GameManager.Instance.CurrentHighscore = orderManager.CurrentScore;
                newHighscoreText.gameObject.SetActive(true);
            }
            else
            {
                newHighscoreText.gameObject.SetActive(false);
            }

            finalScore.SetText(orderManager.CurrentScore + "P");
            currentHighscore.SetText("Current Highscore: " + GameManager.Instance.CurrentHighscore + "P");
            levelEndScreen.SetActive(true);
        }
    }
}
