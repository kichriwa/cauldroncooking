﻿namespace CauldronCooking
{
    public interface IIngredientReplacer
    {
        Ingredient GetNewIngredient(Ingredient oldIngredient);
    }
}
