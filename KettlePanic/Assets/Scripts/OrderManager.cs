﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CauldronCooking
{
    public class OrderManager : MonoBehaviour
    {
        private const int REWARD_PER_RECIPE = 100;
        private const int PUNISHMENT_PER_FAILED_RECIPE = -20;

        [SerializeField] private LineRenderer lineRenderer;
        [SerializeField] private ObjectPool orderPool;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private int leftRightPadding = 20;
        [SerializeField] private float orderSpeed = 2f;


        public bool HasRoomForMoreOrders
        {
            get => currentOrders.Count < orderPool.ObjectAmount;
        }

        private List<Recipe> levelRecipes;
        private List<Order> currentOrders;
        private WaitForSeconds recipeDelayTimeout;
        private float recipeFailTime;
        public int CurrentScore { get; private set; }
        private List<float> xPositions = new List<float>();

        private Coroutine currentOrderCoroutine;

        private void Start()
        {
            Order.failedRecipe += HandleFailedRecipe;
            UpdatePoints(0);
            InitializeXPositions();
        }

        public void ResetOrders()
        {
            UpdatePoints(0);

            if (currentOrderCoroutine != null)
            {
                StopCoroutine(currentOrderCoroutine);
            }

            currentOrders.ForEach(order => order.gameObject.SetActive(false));
            currentOrders = new List<Order>();
        }

        public void InitializeXPositions()
        {
            Vector3 firstPosition = lineRenderer.GetPosition(1);
            Vector3 lastPosition = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
            float distance = lastPosition.x - firstPosition.x;
            float spacePerRecipe = (distance - 2 * leftRightPadding) / orderPool.ObjectAmount;

            xPositions.Add(firstPosition.x + leftRightPadding + spacePerRecipe / 2);

            for (var i = 1; i < orderPool.ObjectAmount; i++)
            {
                xPositions.Add(xPositions[i - 1] + spacePerRecipe);
            }
        }

        public void Initialize(List<Recipe> recipes, float recipeDelay, float recipeFailTime)
        {
            levelRecipes = recipes;
            currentOrders = new List<Order>();
            recipeDelayTimeout = new WaitForSeconds(recipeDelay);
            this.recipeFailTime = recipeFailTime;
        }

        public void MakeNewOrder()
        {
            Recipe chosenRecipe = levelRecipes[Random.Range(0, levelRecipes.Count)];
            GameObject orderObject = orderPool.GetObject();
            Order orderComponent = orderObject.GetComponent<Order>();
            orderComponent.Initialize(chosenRecipe, recipeFailTime);
            orderComponent.OrderIndex = -1;
            currentOrders.Add(orderComponent);
            orderObject.SetActive(true);
            orderComponent.StartTimeOut();
            UpdateOrderIndicesOfOrders();
            StartCoroutine(SetNewOrderToStartPositionAndMoveAll(orderComponent));
        }

        public void ResolveNewBroughtObject(GameObject sender, PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerDrag.CompareTag("Bottle"))
            {
                BottleManager bottle = pointerEventData.pointerDrag.GetComponentInChildren<BottleManager>();
                if (bottle.currentRecipe != null)
                {
                    if (ResolveNewRecipe(bottle.currentRecipe))
                    {
                        sender.GetComponentInChildren<Image>(true).overrideSprite = bottle.GetCurrentSprite();
                        sender.GetComponent<Animator>().SetTrigger("Received Recipe");

                        if (currentOrders.Count > 0)
                        {
                            UpdateOrderIndicesOfOrders();
                            MoveAllRecipesToNewPlace();
                        }

                        if (currentOrderCoroutine == null)
                        {
                            StartOrderingCoroutine();
                        }
                    }
                }
            }
        }

        public void StartOrderingCoroutine()
        {
            if (currentOrderCoroutine != null)
            {
                StopCoroutine(currentOrderCoroutine);
            }

            currentOrderCoroutine = StartCoroutine(MakeOrders());
        }


        private IEnumerator MakeOrders()
        {
            while (HasRoomForMoreOrders)
            {
                yield return recipeDelayTimeout;
                MakeNewOrder();
            }

            currentOrderCoroutine = null;
        }

        private void UpdatePoints(int newPoints)
        {
            CurrentScore = newPoints;
            scoreText.SetText(newPoints + "P");
        }

        private bool ResolveNewRecipe(Recipe recipe)
        {
            for (var i = 0; i < currentOrders.Count(); i++)
            {
                if (currentOrders[i].Recipe.Equals(recipe))
                {
                    currentOrders[i].gameObject.SetActive(false);
                    UpdatePoints(
                        Mathf.FloorToInt(CurrentScore + (REWARD_PER_RECIPE * currentOrders[i].RewardMultiplier)));
                    ((IList) currentOrders).RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        private void HandleFailedRecipe(object sender, EventArgs _)
        {
            UpdatePoints(CurrentScore + PUNISHMENT_PER_FAILED_RECIPE);
        }

        private void UpdateOrderIndicesOfOrders()
        {
            var limit = Math.Max(currentOrders.Count - 1, currentOrders[0].OrderIndex);

            for (var i = 0; i < currentOrders.Count; i++)
            {
                currentOrders[i].OrderIndex = limit - i;
            }
        }

        private IEnumerator SetNewOrderToStartPositionAndMoveAll(Order orderComponent)
        {
            yield return StartCoroutine(orderComponent.SetToStartPosition(lineRenderer.GetPosition(0)));
            MoveAllRecipesToNewPlace();
        }

        private void MoveAllRecipesToNewPlace()
        {
            for (var i = 0; i < currentOrders.Count; i++)
            {
                var currentOrder = currentOrders[i];
                currentOrder.MoveRecipeToDestination(xPositions[currentOrders[i].OrderIndex], lineRenderer, orderSpeed);
            }
        }

        private void OnDestroy()
        {
            Order.failedRecipe -= HandleFailedRecipe;
        }
    }
}