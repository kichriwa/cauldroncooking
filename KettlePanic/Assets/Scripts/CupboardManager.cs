﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CauldronCooking
{
    public class CupboardManager : MonoBehaviour, IIngredientReplacer
    {
        [SerializeField] private int amountOfItemsPerCupboard;
        [SerializeField] private GameObject itemPrefab;
        [SerializeField] private List<Transform> cupboards;

        private IngredientAmountDictionary currentIngredientsAmount = new IngredientAmountDictionary();

        public void InitializeCupboards(List<Recipe> recipes)
        {
            InitializeIngredientAmountDictionary(recipes);
            FillCupboards();
        }

        private void FillCupboards()
        {
            int ingredientAmount = cupboards.Count * amountOfItemsPerCupboard;

            if (ingredientAmount < currentIngredientsAmount.Count)
            {
                Debug.LogError(
                    "To few ingredient places. It's impossible to display one of each kind. Game is not playable!");
                gameObject.SetActive(false);
                return;
            }

            Ingredient[] ingredientsPerPlace = new Ingredient[ingredientAmount];
            Ingredient[] ingredientOptions = currentIngredientsAmount.Keys.Where(i => i.GetType() != typeof(KombiIngredient)).ToArray();
            var index = 0;

            // To ensure that every ingredient is at least one time available
            foreach (var ingredient in ingredientOptions)
            {
                ingredientsPerPlace[index] = ingredient;
                currentIngredientsAmount[ingredient]++;
                index++;
            }

            // Fill cupboards with random other ingredients
            for (; index < ingredientAmount; index++)
            {
                var ingredient = ingredientOptions[Random.Range(0, ingredientOptions.Length)];
                ingredientsPerPlace[index] = ingredient;
                currentIngredientsAmount[ingredient]++;
            }

            ingredientsPerPlace = ingredientsPerPlace.Shuffle();

            var cupboardIndex = 0;
            foreach (var cupboard in cupboards)
            {
                for (int i = 0; i < amountOfItemsPerCupboard; i++)
                {
                    GameObject instatiatedItem = Instantiate(itemPrefab, cupboard);
                    instatiatedItem.GetComponent<Item>().SetNewIngredient(ingredientsPerPlace[(cupboardIndex * amountOfItemsPerCupboard)+i]);
                }

                cupboardIndex++;
            }
        }

        private void InitializeIngredientAmountDictionary(List<Recipe> recipes)
        {
            foreach (var recipe in recipes)
            {
                foreach (var ingredient in recipe.Ingredients.Keys)
                {
                    if (ingredient is KombiIngredient kombi)
                    {
                        if (!currentIngredientsAmount.ContainsKey(kombi.ingredient1))
                        {
                            currentIngredientsAmount.Add(kombi.ingredient1, 0);
                        }

                        if (!currentIngredientsAmount.ContainsKey(kombi.ingredient2))
                        {
                            currentIngredientsAmount.Add(kombi.ingredient2, 0);
                        }
                    }
                    else
                    {
                        if (!currentIngredientsAmount.ContainsKey(ingredient))
                        {
                            currentIngredientsAmount.Add(ingredient, 0);
                        }
                    }

                }
            }
        }

        public Ingredient GetNewIngredient(Ingredient formerIngredient)
        {
            if(currentIngredientsAmount[formerIngredient] == 1)
            {
                return formerIngredient;
            }

            currentIngredientsAmount[formerIngredient]--;
            var ingredientOptions = currentIngredientsAmount.Keys.Where(i => i.GetType() != typeof(KombiIngredient)).ToArray();
            var newIngredient = ingredientOptions[Random.Range(0, ingredientOptions.Count())];
            currentIngredientsAmount[newIngredient]++;
            return newIngredient;
        }
    }
}
