﻿using System.Collections.Generic;
using UnityEngine;

namespace CauldronCooking
{
    public class DataManager
    {
        private const string INGREDIENT_IMAGE_PATH = "Sprites/Ingredients";
        private const string RECIPE_SYMBOLS_IMAGE_PATH = "Sprites/RecipeSymbols";
        private const string RECIPES_IMAGE_PATH = "Sprites/Recipes";
        private const string LEVEL_DATA_PATH = "Data/Level";
        private const string KOMBI_INGREDIENT_DATA_PATH = "Data/Kombi";

        public AtlasManager IngredientAtlas { get; }
        public AtlasManager RecipeSymbolAtlas { get; }
        public AtlasManager RecipeAtlas { get;  }
        public KombiIngredient[] KombiIngredients { get; }
        public Level[] Levels { get; }

        public DataManager()
        {
            IngredientAtlas = new AtlasManager(INGREDIENT_IMAGE_PATH);
            RecipeSymbolAtlas = new AtlasManager(RECIPE_SYMBOLS_IMAGE_PATH);
            RecipeAtlas = new AtlasManager(RECIPES_IMAGE_PATH);
            KombiIngredients = Resources.LoadAll<KombiIngredient>(KOMBI_INGREDIENT_DATA_PATH);
            Levels = Resources.LoadAll<Level>(LEVEL_DATA_PATH);
        }

        public Recipe GetRandomRecipe(int level)
        {
            List<Recipe> recipes = Levels[level].recipes;

            return recipes[Random.Range(0, recipes.Count - 1)];
        }
    }
}
