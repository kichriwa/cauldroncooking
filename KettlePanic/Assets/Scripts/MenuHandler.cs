﻿using TMPro;
using UnityEngine;

namespace CauldronCooking
{
    public class MenuHandler : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI highscore;

        private void Start()
        {
            SetNewHighscore();
        }

        public void StartGame()
        {
            GameManager.Instance.SwitchToScene(1);
        }

        public void ExitGame()
        {
            GameManager.Instance.ExitGame();
        }

        public void ResetHighscore()
        {
            GameManager.Instance.ResetHighscore();
            SetNewHighscore();
        }

        private void SetNewHighscore()
        {
            highscore.SetText("Current Highscore - " + GameManager.Instance.CurrentHighscore + "P");
        }

    }
}
