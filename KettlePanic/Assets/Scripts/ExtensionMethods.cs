﻿using System;

namespace CauldronCooking
{
    public static class ExtensionMethods
    {
        public static TSource[] Shuffle<TSource>(this TSource[] array)
        {
            UnityEngine.Random.InitState(DateTime.Now.GetHashCode());
            int n = array.Length;
            TSource[] copy = new TSource[n];
            array.CopyTo(copy, 0);
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                if (k == n) { continue; }
                (copy[k], copy[n]) = (copy[n], copy[k]);
            }
            return copy;
        }
    }
}
