﻿using System;
using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace CauldronCooking
{
    /*
    * Handles the movement and dispaly of the game object for the order
    */
    public class Order : MonoBehaviour
    {
        // Event that can be listened to for example to decrease score
        public static event EventHandler failedRecipe;
        [SerializeField] private Image recipeImage;
        [SerializeField] private List<GameObject> ingredientImagesParents;
        // Sliders that are used to display the time left
        [SerializeField] private List<Slider> sliders;

        public Recipe Recipe { get; private set; }
        public float RewardMultiplier { get; private set; }
        public int OrderIndex { get; set; }

        private int currentGoalWayPointIndex = 1;
        private Coroutine currentMoveCoroutine;
        private Coroutine currentTimeCoroutine;
        private float recipeFailTime;

        /* Order objects are reused within an object pool the initialize method resets data
        *  and prepares the object for a new use
        */
        public void Initialize(Recipe recipe, float recipeFailTime)
        {
            currentGoalWayPointIndex = 1;
            RewardMultiplier = 1;
            Recipe = recipe;
            // Reset sliders displaying time left
            ResetAllSliders();
            // Makes fail time flexible to be controlled by higher manager class
            this.recipeFailTime = recipeFailTime;
            // Set images
            SetRecipeImage(recipe.id);
            SetIngredientImages(recipe.Ingredients);
        }

        /*
        * Resets all sliders so that the recipe appears to have
        * full working time again
        */
        private void ResetAllSliders()
        {
            sliders.ForEach(slider => slider.value = 1);
        }

        /*
        * Starts coroutine that keeps track of time
        */
        public void StartTimeOut()
        {
            if (currentTimeCoroutine != null)
            {
                StopCoroutine(currentTimeCoroutine);
            }

            currentTimeCoroutine = StartCoroutine(CountTime());
        }

        /*
        * Replaces the recipe image by requestign it from a self written 
        * sprite atlas class
        */
        private void SetRecipeImage(string recipeId)
        {
            Sprite recipeSprite;

            if (!GameManager.Instance.dataManager.RecipeSymbolAtlas.TryGetSprite(recipeId, out recipeSprite))
            {
                Debug.LogWarning($"Recipe image for '{recipeId}' could not be found.");
            }

            recipeImage.overrideSprite = recipeSprite;
        }

        /*
        * Object is moved to object pool on disable
        * and all coroutines are stopped 
        */
        private void OnDisable()
        {
            if (currentMoveCoroutine != null)
            {
                StopCoroutine(currentMoveCoroutine);
            }

            if (currentTimeCoroutine != null)
            {
                StopCoroutine(currentTimeCoroutine);
            }
        }

        /*
        * Sets images for the necessary incredients
        */
        private void SetIngredientImages(IngredientAmountDictionary ingredientAmounts)
        {
            // Select ingredients that are a combination of others
            var kombiIngredientKeys = ingredientAmounts.Keys.OfType<KombiIngredient>();

            // Set images for ingredients that need to be added to the cauldron
            InitializeTransformWithChildren(ingredientImagesParents[0],
                ingredientAmounts.ToDictionary(kvp => kvp.Key, kvp => kvp.Value), false);

            Dictionary<Ingredient, int> ingredientsUsedForKombi = new Dictionary<Ingredient, int>();

            /* If there are ingredients that are a combination of others set images for the
            * ingredients that are used to merge them into the combination.
            */
            if (kombiIngredientKeys.Count() > 0)
            {
                var kombiIngredient = kombiIngredientKeys.First();
                
                // For display reasons only one kombi ingredient can be displayed right now
                if (kombiIngredientKeys.Count() > 1 || ingredientAmounts[kombiIngredient] > 1)
                {
                    Debug.LogError("More than one kombi ingredient is not supported as of now.");
                    return;
                }

                ingredientImagesParents[1].SetActive(true);

                /* Set amount of kombiingredient to 2 if both ingredients are the same or the count of both ingredients to
                *  1 if they are different.
                * This is necessary to treat them as other ingredients in the InitializeTransformWithChildren call.
                */
                if (kombiIngredient.ingredient1.Equals(kombiIngredient.ingredient2))
                {
                    ingredientsUsedForKombi[kombiIngredient.ingredient1] = 2;
                }
                else
                {
                    ingredientsUsedForKombi[kombiIngredient.ingredient1] = 1;
                    ingredientsUsedForKombi[kombiIngredient.ingredient2] = 1;
                }

                InitializeTransformWithChildren(ingredientImagesParents[1], ingredientsUsedForKombi, true);
            }
            else
            {
                /* Deactivate ingredient images that are used to merge into a combination as there are
                * no combinational ingredients.
                */
                ingredientImagesParents[1].SetActive(false);
            }
        }

        /*
        * Changes the images in the child game objects of the given ingredientImageParent to 
        * fit the given Ingredients and amounts and decides whether all ingredients should have
        * their background images that indicates portal usage activated  
        */
        private void InitializeTransformWithChildren(GameObject ingredientImageParent,
            Dictionary<Ingredient, int> ingredientAmounts, bool portalImageForAll)
        {
            var ingredients = ingredientAmounts.Keys.ToList();
            var currentIngredientIndex = 0;
            var currentAmount = 0;
            Ingredient currentIngredient = null;
            Sprite currentIngredientSprite = null;

            // Go over all child objects of the given ingredientImageParent
            foreach (Transform ingredientImageTransform in ingredientImageParent.transform)
            {
                if (currentIngredientIndex < ingredients.Count)
                {
                    ingredientImageTransform.gameObject.SetActive(true);

                    /* Only select new image if a new ingredient is looked at.
                    * If count is greater than 0, the current sprite can be reused
                    */
                    if (currentAmount == 0)
                    {
                        currentIngredient = ingredients[currentIngredientIndex];
                        
                        // Tries to get the ingredient image from the self written sprite atlas class
                        if (!GameManager.Instance.dataManager.IngredientAtlas.TryGetSprite(currentIngredient.Id,
                            out currentIngredientSprite))
                        {
                            Debug.LogWarning($"No image could be found for ingredient '{currentIngredient.Id}'");
                            return;
                        }
                    }

                    // Selects the image component of the currently referenced transform
                    var images = ingredientImageTransform.GetComponentsInChildren<Image>();
                    
                    /* Checks whether portal image for all is active or we are dealing with kombi ingredient and
                    * sets color respectively
                    */
                    if (portalImageForAll || currentIngredient.GetType() == typeof(KombiIngredient))
                    {
                        images[0].color = new Color(1, 1, 1, 0.6f);
                    }
                    else
                    {
                        images[0].color = new Color(1, 1, 1, 0);
                    }

                    // Sets new image
                    images[1].overrideSprite = currentIngredientSprite;
                    currentAmount++;

                    if (currentAmount >= ingredientAmounts[currentIngredient])
                    {
                        currentAmount = 0;
                        currentIngredientIndex++;
                    }
                }
                else
                {
                    ingredientImageTransform.gameObject.SetActive(false);
                }
            }
        }

        /*
        * Sets the order object to the given position and enforces layout update
        */
        public IEnumerator SetToStartPosition(Vector3 startPosition)
        {
            OrderIndex = 0;
            // To update UI
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
            RectTransform rect = GetComponent<RectTransform>();
            // Needed or else the order object may not adjusted it's sizing yet
            yield return new WaitForLayoutCompletion();
            rect.anchoredPosition = startPosition;
        }

        /*
        * Starts the coroutine that lets the order game object move along the linerenderer until it reaches 
        * the x position of xPositionGoal
        */
        public void MoveRecipeToDestination(float xPositionGoal, LineRenderer lineRenderer, float orderSpeed)
        {
            if (currentMoveCoroutine != null)
            {
                StopCoroutine(currentMoveCoroutine);
            }

            currentMoveCoroutine = StartCoroutine(MoveTowardsNextPosition(xPositionGoal, lineRenderer, orderSpeed));
        }

        /*
        * Keeps track of the time, updates the sliders and sends the failed recipe event
        * if time is up
        */
        private IEnumerator CountTime()
        {
            float currentTime = 0;
            int currentSliderIndex = 0;
            float timePerSlider = recipeFailTime / sliders.Count;

            while (recipeFailTime - currentTime >= 0.1)
            {
                currentTime += Time.deltaTime;
                int sliderIndex = Mathf.FloorToInt(currentTime / timePerSlider);

                if (sliderIndex != currentSliderIndex)
                {
                    sliders[currentSliderIndex].value = 0;
                    RewardMultiplier = GetNewRewardMultiplier(sliderIndex);
                }

                currentSliderIndex = sliderIndex;
                sliders[currentSliderIndex].value = (timePerSlider - (currentTime - (currentSliderIndex * timePerSlider))) / timePerSlider;
                yield return null;
            }

            RewardMultiplier = 0f;
            sliders[sliders.Count - 1].value = 0;

            failedRecipe?.Invoke(this, null);
        }


        /*
        * Determines based on the currently outrunning slider how much of the points
        * the player will still get when fulfilling the order
        */
        private float GetNewRewardMultiplier(int sliderIndex)
        {
            switch (sliderIndex)
            {
                case 0: return 1;
                case 1: return 0.8f;
                case 2: return 0.4f;
                case 3: return 0.2f;
                default: return 0f;
            }
        }

        /*
        * Moves along the line renderer until xPositionGoal is reached. 
        */
        private IEnumerator MoveTowardsNextPosition(float xPositionGoal, LineRenderer lineRenderer, float orderSpeed)
        {
            RectTransform rect = GetComponent<RectTransform>();

            /*
            * Current goal is next edge on line renderer
            */
            Vector3 currentGoal = lineRenderer.GetPosition(currentGoalWayPointIndex); 


            while (Mathf.Abs(xPositionGoal - rect.anchoredPosition.x) > 0.1f)
            {
                float step = orderSpeed * Time.deltaTime;
                var newPosition = Vector3.MoveTowards(rect.anchoredPosition, currentGoal, step);

                if (newPosition.x > xPositionGoal)
                {
                    newPosition.x = xPositionGoal;
                    break;
                }

                rect.anchoredPosition = newPosition;
   
                // Updates the index of the edge of the line renderer to use
                if (Vector3.Distance(rect.anchoredPosition, currentGoal) < 0.2f)
                {
                    currentGoalWayPointIndex++;
                    currentGoal = lineRenderer.GetPosition(currentGoalWayPointIndex);
                }


                yield return null;
            }

            currentMoveCoroutine = null;
        }
    }

    /*
    * For sorting collections that contain order objects
    */
    class OrderComparer : IComparer<Order>
    {
        public int Compare(Order x, Order y)
        {
            if (x == null)
            {
                return 1;
            }

            if (y == null)
            {
                return 1;
            }

            if (x.OrderIndex > y.OrderIndex)
            {
                return -1;
            }

            if (x.OrderIndex < y.OrderIndex)
            {
                return 1;
            }

            return 0;
        }
    }
}