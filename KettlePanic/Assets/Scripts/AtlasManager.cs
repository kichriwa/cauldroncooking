﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CauldronCooking
{
    public class AtlasManager
    {
        private readonly Dictionary<int, Sprite> spritesNumberWise = new Dictionary<int, Sprite>();
        private readonly Dictionary<string, Sprite> spritesStringWise = new Dictionary<string, Sprite>();

        public AtlasManager(string atlasPath, bool buildIntDictionaryAdditional = false, bool fallbackOnCounterVariable = true) : this(Resources.LoadAll<Sprite>(atlasPath).ToList(), buildIntDictionaryAdditional,
            fallbackOnCounterVariable)
        {
        }

        public AtlasManager(List<Sprite> atlasSprite, bool buildIntDictionaryAdditional = false, bool fallbackOnCounterVariable = true)
        {
            int counter = 0;
            int i = 0;
            foreach (Sprite s in atlasSprite)
            {
                if (buildIntDictionaryAdditional)
                {
                    if (int.TryParse(s.name, out i))
                    {
                        spritesNumberWise[i] = s;
                        counter = i + 1;
                    }
                    else if (fallbackOnCounterVariable)
                    {
                        spritesNumberWise[counter] = s;
                        counter++;
                    }

                }

                spritesStringWise[s.name] = s;
            }
        }

        public bool TryGetSprite(string spriteName, out Sprite sprite)
        {
            return spritesStringWise.TryGetValue(spriteName, out sprite);
        }

        public bool TryGetSprite(int spriteId, out Sprite sprite)
        {
            return spritesNumberWise.TryGetValue(spriteId, out sprite);
        }
    }
}
