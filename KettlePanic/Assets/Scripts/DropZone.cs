﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace CauldronCooking
{
    public class DropZone : MonoBehaviour, IDropHandler
    {
        [FormerlySerializedAs("pointerEvent")] [SerializeField] private PointerEvent dropEvents;

        public void OnDrop(PointerEventData eventData)
        {
            dropEvents.Invoke(gameObject, eventData);
        }
    }

    [System.Serializable]
    public class PointerEvent : UnityEvent<GameObject, PointerEventData>
    {

    }
}

