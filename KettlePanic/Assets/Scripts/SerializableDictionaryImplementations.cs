using System;
using System.Collections.Generic;
using CauldronCooking;
using RotaryHeart.Lib.SerializableDictionary;

// ---------------
//  Ingredient => int
// ---------------
[Serializable]
public class IngredientAmountDictionary : SerializableDictionaryBase<Ingredient, int>
{
    public override bool Equals(object obj)
    {
        Dictionary<Ingredient, int> dict = null;

        if (obj is IngredientAmountDictionary)
        {
            dict = ((IngredientAmountDictionary)obj)._dict;
        }

        if (obj is Dictionary<Ingredient, int>)
        {
            dict = (Dictionary<Ingredient, int>) obj;
        }

        if (dict != null)
        {
            if(dict.Keys.Count != _dict.Keys.Count) { return false;}

            int currentValue;

            foreach (var key in dict.Keys)
            {
                bool success = _dict.TryGetValue(key, out currentValue);

                if (!success) { return false; }
                if (currentValue != dict[key]) { return false; }
            }

            return true;
        }

        return false;
    }

    public override int GetHashCode()
    {
        if (_dict != null)
        {
            return _dict.GetHashCode();
        }

        return -1;
    }
}

