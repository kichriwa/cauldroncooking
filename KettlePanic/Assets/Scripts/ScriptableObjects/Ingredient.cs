﻿using UnityEngine;

namespace CauldronCooking
{
    [CreateAssetMenu(fileName = "Ingredient", menuName = "KettlePanic/Ingredient", order = 1)]
    public class Ingredient : ScriptableObject
    {
        [SerializeField]
        private string _id;

        public string Id => _id;
    }
}
