﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CauldronCooking
{
    [CreateAssetMenu(fileName = "Level", menuName = "KettlePanic/Level", order = 1)]
    public class Level : ScriptableObject
    {
        public int levelNumber;
        public List<Recipe> recipes;
        public float recipeDelay;
        public float failedTime;
    }
}
