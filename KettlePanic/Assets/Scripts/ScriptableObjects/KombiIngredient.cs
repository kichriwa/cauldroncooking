﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CauldronCooking
{
    [CreateAssetMenu(fileName = "New Kombi", menuName = "KettlePanic/Kombi", order = 1)]
    public class KombiIngredient : Ingredient
    {
        public Ingredient ingredient1;
        public Ingredient ingredient2;

        public bool ConsistsOfIngredients(Ingredient ingredient1, Ingredient ingredient2)
        {
            return (this.ingredient1.Equals(ingredient1) && this.ingredient2.Equals(ingredient2)) ||
                   (this.ingredient2.Equals(ingredient1) && this.ingredient1.Equals(ingredient2));
        }

    }
}
