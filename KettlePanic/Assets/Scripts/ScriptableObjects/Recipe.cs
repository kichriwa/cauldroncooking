﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CauldronCooking
{
    [CreateAssetMenu(fileName = "Recipe", menuName = "KettlePanic/Recipe", order = 2)]
    public class Recipe : ScriptableObject, IEquatable<Recipe>, IEquatable<IngredientAmountDictionary>, IEquatable<Dictionary<Ingredient, int>>
    {
        public string id;
        [SerializeField]
        private IngredientAmountDictionary _ingredients;

        public IngredientAmountDictionary Ingredients => _ingredients;

        public bool Equals(IngredientAmountDictionary other)
        {
            return Ingredients.Equals(other);
        }

        public bool Equals(Recipe other)
        {
            return other != null && other.id.Equals(this.id);
        }

        public bool Equals(Dictionary<Ingredient, int> other)
        {
            return Ingredients.Equals(other);
        }

        public override bool Equals(object obj)
        {
            if (obj is Recipe)
            {
                return Equals((Recipe)obj);
            }

            if (obj is IngredientAmountDictionary)
            {
                return Equals((IngredientAmountDictionary) obj);
            }

            if (obj is Dictionary<Ingredient, int>)
            {
                return Equals((Dictionary<Ingredient, int>) obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Ingredients.GetHashCode();
        }
    }
}
