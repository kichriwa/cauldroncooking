﻿using System.Collections.Generic;
using UnityEngine;

namespace CauldronCooking
{
    public class ObjectPool : MonoBehaviour
    {
        public GameObject Prefab { get => _prefab; set => _prefab = value; }
        [SerializeField] private GameObject _prefab;

        public int ObjectAmount { get => _objectAmount; set => _objectAmount = value; }
        [SerializeField] private int _objectAmount;

        [SerializeField] private bool initializeOnAwake = false;

        [System.NonSerialized] public Transform transformParent;

        private List<GameObject> objectPool;

        public void Awake()
        {
            if (initializeOnAwake)
            {
                InitializePool();
            }
        }

        public void InitializePool()
        {
            InitializePool(transform);
        }

        public void InitializePool(Transform parent)
        {
            transformParent = parent;
            objectPool = new List<GameObject>();
            Prefab.SetActive(false);

            for (var i = 0; i <= ObjectAmount; i++)
            {
                objectPool.Add(Instantiate(Prefab, parent, true));
            }
        }

        public GameObject GetObject()
        {
            foreach (var gameObject in objectPool)
            {
                if (!gameObject.activeSelf)
                {
                    return gameObject;
                }
            }

            AddToPool();

            Debug.LogWarning($"Not enough game objects in object pool on gameObject '{gameObject.name}' to spawn");

            return objectPool[objectPool.Count - 1];
        }

        public void DeactivateAll()
        {
            foreach (var gameObject in objectPool)
            {
                gameObject.SetActive(false);
            }
        }

        private void AddToPool()
        {
            objectPool.Add(Instantiate(Prefab, transformParent, true));
        }
    }
}
