﻿using UnityEngine;
using UnityEngine.UI;

namespace CauldronCooking
{
    [RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
    public class Item : MonoBehaviour {
        public Ingredient Ingredient { get; protected set; }

        protected Animator anim;
        protected Image image;
        private GameObject dragImageObject;
        private Image dragImage;

        private Camera cam;
        private IIngredientReplacer ingredientReplacer;

        void Awake()
        {
            OnValidate();
        }

        public void OnValidate()
        {
            dragImageObject = transform.GetChild(0).gameObject;
            image = GetComponent<Image>();
            dragImage = dragImageObject.GetComponent<Image>();
            ingredientReplacer = GetComponentInParent<IIngredientReplacer>();
            anim = GetComponent<Animator>();
        }

        public void SetNewIngredient(Ingredient ingredient)
        {
            Sprite ingredientSprite;

            if (!GameManager.Instance.dataManager.IngredientAtlas.TryGetSprite(ingredient.Id, out ingredientSprite))
            {
                Debug.LogError("There is no corresponding image for ingredient id" + ingredient.Id);
                return;
            }

            Ingredient = ingredient;
            image.overrideSprite = ingredientSprite;
            dragImage.overrideSprite = ingredientSprite;
        }

        public void GotUsed(string animationParameter = "Vanish")
        {
            dragImageObject.SetActive(false);
            anim.SetTrigger(animationParameter);
        }

        public void ChangeIngredient()
        {
            SetNewIngredient(ingredientReplacer.GetNewIngredient(Ingredient));
            anim.SetTrigger("Reappear");
        }

        public void SetCurrentlyDragged(bool currentlyDragged)
        {
            anim.SetBool("CurrentlyDragged", currentlyDragged);
        }
        
        
    }
}
