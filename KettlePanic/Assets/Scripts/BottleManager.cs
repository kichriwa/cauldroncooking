﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CauldronCooking
{
    public class BottleManager : MonoBehaviour
    {
        [NonSerialized] public Recipe currentRecipe;
        private Image img;
        private Sprite defaultImg;

        private void Awake()
        {
            Validate();
        }

        private void Validate()
        {
            img = GetComponent<Image>();
            defaultImg = img.sprite;
        }
        public void SetRecipe(Recipe recipe)
        {
            currentRecipe = recipe;

            if (currentRecipe != null)
            {
                Sprite bottleImage;
                if (GameManager.Instance.dataManager.RecipeAtlas.TryGetSprite(currentRecipe.id, out bottleImage))
                {
                    img.overrideSprite = bottleImage;
                }
            }
        }

        public Sprite GetCurrentSprite()
        {
            return img.overrideSprite;
        }

        public void ResetRecipe()
        {
            currentRecipe = null;
            img.overrideSprite = defaultImg;
        }
    }
}

