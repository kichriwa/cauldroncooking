﻿using UnityEngine;

public class AspectRatioEntforcer : MonoBehaviour
{
    [SerializeField] private RectTransform reference;
    private float currentAspectRatio;
    private Camera cameraReference;

    private void Awake()
    {
        cameraReference = GetComponent<Camera>();
        FixCameraAspectRatioOnBackground();
    }

    private void Update()
    {
        if (Mathf.Abs(cameraReference.aspect - currentAspectRatio) >= 0.01f)
        {
            FixCameraAspectRatioOnBackground();
        }
    }

    private void FixCameraAspectRatioOnBackground()
    {
        if (cameraReference.aspect < 1.77f)
        {
            cameraReference.orthographicSize = reference.rect.width * reference.localScale.x / 2f / cameraReference.aspect;
        }
        else
        {
            cameraReference.orthographicSize = reference.rect.height * reference.localScale.y / 2f;
        }
        currentAspectRatio = cameraReference.aspect;
    }
}
