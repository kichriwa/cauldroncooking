﻿namespace CauldronCooking
{
    public class PortalIngredient : Item
    {

        public void Deactivate()
        {
            Ingredient = null;
            GetComponentInChildren<DragLogic>(true).DragBlocked = true;
            image.enabled = false;
            anim.enabled = false;
            enabled = false;
        }

        public void Activate()
        {
            image.enabled = true;
            anim.enabled = true;
            enabled = true;
        }
    }
}
