﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace CauldronCooking
{
    [RequireComponent(typeof(Image))]
    public class DissolveShaderControl : MonoBehaviour
    {
        public const string MATERIAL_NAME = "DissolveMaterial";

        private Image Image { get; set; }

        private Material imageMaterial;

        public float dissolveAmount;

        private static readonly int spriteTextureId = UnityEngine.Shader.PropertyToID("_MainTex");
        private static readonly int dissolveAmountId = UnityEngine.Shader.PropertyToID("_Dissolve");


        private void Awake()
        {
            Image = GetComponent<Image>();
        }


        private void OnValidate()
        {
            if (!isActiveAndEnabled)
            {
                return;
            }

            SetMaterialProperties();
        }

        private void OnDisable()
        {
            dissolveAmount = 1;
            SetMaterialProperties();
        }

        private void Update()
        {
            SetMaterialProperties();
        }

        private void SetMaterialProperties()
        {
            //Debug.Log("Called set materials");
            if (!Image) return;

#if UNITY_EDITOR
            if (Image.material == null || !Image.material.name.Equals(MATERIAL_NAME))
            {
                if (imageMaterial == null)
                {
                    imageMaterial = AssetDatabase.LoadAssetAtPath<Material>($"Assets/Materials/{MATERIAL_NAME}.mat");
                }

                Image.material = imageMaterial;
            }
#else
            if (!Image.material.name.Equals(MATERIAL_NAME))
            {
                Debug.LogWarning($"Material of SpriteRenderer not set to {MATERIAL_NAME}. Please make sure this component only is attached to a sprite with this material.");
                return;
            }
#endif

            Material mat = Instantiate(Image.material);

            mat.SetTexture(spriteTextureId, Image.sprite.texture);
            mat.SetFloat(dissolveAmountId, dissolveAmount);

            Image.material = mat;
            Image.SetMaterialDirty();
        }
    }
}