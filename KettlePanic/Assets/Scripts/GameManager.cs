﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CauldronCooking
{
    public class GameManager : MonoBehaviour
    {
        private const int MAXIMAL_RECIPES = 5;
        private const string HIGHSCORE_KEY = "Highscore";
        private static GameManager _instance;

        public static GameManager Instance
        {
            get { return _instance; }

            private set { _instance = value; }
        }


        public DataManager dataManager { get; private set; }
        public int CurrentHighscore
        {
            get { return _currentHighscore; }
            set
            {
                _currentHighscore = value;
                PlayerPrefs.SetInt(HIGHSCORE_KEY, value);
            }
        }

        private int _currentHighscore;
        [SerializeField] private float fadeDuration = 4f;
        [SerializeField] private CanvasGroup fadeImage;

        private int currentLevel = 0;
        private List<Recipe> currentRecipe;

        private Coroutine switchSceneCoroutine;
        private Coroutine fadeCoroutine;

        public void Awake()
        {
            dataManager = new DataManager();

            if (_instance != null)
            {
                Destroy(fadeImage.transform.parent.gameObject);
                Destroy(gameObject);
                return;
            }

            _currentHighscore = PlayerPrefs.GetInt("Highscore", 0);
            _instance = this;

            DontDestroyOnLoad(this);
            DontDestroyOnLoad(fadeImage.transform.parent);
        }

        public Level GetLevelData()
        {
            return dataManager.Levels[currentLevel];
        }

        public void ResetHighscore()
        {
            CurrentHighscore = 0;
            PlayerPrefs.DeleteKey(HIGHSCORE_KEY);
        }

        public void SwitchToScene(int sceneBuildIndex)
        {
            if (switchSceneCoroutine != null)
            {
                StopCoroutine(switchSceneCoroutine);
            }

            StartCoroutine(FadeAndSwitchScenes(sceneBuildIndex));
        }

        /*
   *  Manages the fading and scene switching process over time via Coroutines
   *
   *  param destinationScene: scene to switch to
   *
   *  return IEnumerator: Enumerator of Coroutine will continue on place of last yield return next frame
   */
        private IEnumerator FadeAndSwitchScenes(int sceneBuildIndex)
        {
            //Fade out - alpha = 1
            yield return StartFading(1f);
            yield return SceneManager.LoadSceneAsync(sceneBuildIndex);
            //Fade in - alpha = 0
            yield return StartFading(0f);
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#endif
#if !UNITY_EDITOR
            Application.Quit();
#endif
        }


        private IEnumerator StartFading(float finalAlpha)
        {
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
            }

            yield return fadeCoroutine = StartCoroutine(Fade(finalAlpha));
        }

        private IEnumerator Fade(float finalAlpha)
        {
            if (fadeImage != null)
            {
                //Speed equals Distance over Time
                float fadeSpeed = Mathf.Abs(fadeImage.alpha - finalAlpha) / fadeDuration;
                //To prevent further interactions during sceneChange
                fadeImage.blocksRaycasts = true;

                while (!Mathf.Approximately(fadeImage.alpha, finalAlpha))
                {
                    fadeImage.alpha = Mathf.MoveTowards(fadeImage.alpha, finalAlpha, fadeSpeed * Time.deltaTime);
                    yield return null;
                }

                fadeImage.blocksRaycasts = false;

                yield break;
            }
        }
    }
}
