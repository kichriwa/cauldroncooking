﻿using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CauldronCooking {

    public class PortalManager : MonoBehaviour, IIngredientReplacer
    {
        [SerializeField] private PortalIngredient leftIngredient;
        [SerializeField] private PortalIngredient rightIngredient;
        [SerializeField] private PortalIngredient kombiResult;

        private void Awake()
        {
            leftIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = true;
            rightIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = true;
            kombiResult.GetComponentInChildren<DragLogic>(true).DragBlocked = true;
        }

        public void HandleDroppedItem(GameObject portal, PointerEventData eventData)
        {
            if (eventData.pointerDrag.CompareTag("Ingredient"))
            {
                var item = eventData.pointerDrag.GetComponentInParent<Item>();

                if (portal.name.Equals("Portal-Left"))
                {
                    leftIngredient.SetNewIngredient(item.Ingredient);
                    leftIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = false;
                    leftIngredient.Activate();
                }
                else if (portal.name.Equals("Portal-Right"))
                {
                    rightIngredient.SetNewIngredient(item.Ingredient);
                    rightIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = false;
                    rightIngredient.Activate();
                }

                CheckIfIngredientsCanBeMerged();
                item.GotUsed();
            }
        }

        public void ResetPortals()
        {
            rightIngredient.Deactivate();
            leftIngredient.Deactivate();
            kombiResult.Deactivate();
        }

        private KombiIngredient pendingMergeIngredient;

        private void CheckIfIngredientsCanBeMerged()
        {
            if (leftIngredient.Ingredient == null || rightIngredient.Ingredient == null)
            {
                return;
            }

            foreach (var kombi in GameManager.Instance.dataManager.KombiIngredients)
            {
                if (kombi.ConsistsOfIngredients(leftIngredient.Ingredient, rightIngredient.Ingredient))
                {
                    leftIngredient.GotUsed("VanishToPortal");
                    rightIngredient.GotUsed("VanishToPortal");
                    leftIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = true;
                    rightIngredient.GetComponentInChildren<DragLogic>(true).DragBlocked = true;
                    pendingMergeIngredient = kombi;
                    kombiResult.GetComponentInChildren<DragLogic>(true).DragBlocked = false;
                    kombiResult.ChangeIngredient();
                    return;
                }
            }
        }

        public Ingredient GetNewIngredient(Ingredient oldIngredient)
        {
            kombiResult.Activate();
            var returnIngredient = pendingMergeIngredient;
            pendingMergeIngredient = null;
            return returnIngredient;
        }
    }
}
